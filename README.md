Sprites!
========

Load sprites and create entities in a neat and repeatable way.

The goal is to make something that can be reused maybe via a bundle?


Tasks
=====

- [ ] Load sprite data from config dynamically
- [ ] Check file exists
- [ ] Use Assets
- [ ] Sprite loader
- [ ] Add animation to sprite
- [ ] Store handle in a handy place
- [ ] Render sprite at location