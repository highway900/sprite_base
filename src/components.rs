use amethyst::core::cgmath::{
	Vector3,
	Vector2
};
use amethyst::ecs::{Component, VecStorage};


#[derive(Clone, Debug, PartialEq)]
pub struct CameraManager {
	pub pos: Vector3<f32>,
	pub zoom: Vector2<f32>,
	pub zoom_speed: f32,
	pub tracking_speed: f32,
}

impl CameraManager {
	pub fn new (pos: Vector3<f32>, zoom: Vector2<f32>, zoom_speed: f32, tracking_speed: f32) -> CameraManager {
		CameraManager {
			pos: pos,
			zoom: zoom,
			zoom_speed: zoom_speed,
			tracking_speed: tracking_speed,
		}
	}
}

impl Component for CameraManager {
    type Storage = VecStorage<Self>;
}

#[derive(Clone, Debug, PartialEq)]
pub struct Movement {
	pub direction: f32,
	pub speed: f32,
}

impl Movement {
	pub fn new (direction: f32, speed: f32) -> Movement {
		Movement {
			direction: direction,
			speed: speed,
		}
	}
}

impl Component for Movement {
    type Storage = VecStorage<Self>;
}