use std::path::Path;

use amethyst::{
    utils::application_root_dir
};

pub fn check_asset_path_exists(path: &String) -> bool {
	let p = format!("{}/assets/{}", application_root_dir(), path);
	Path::new(&p).exists()
}