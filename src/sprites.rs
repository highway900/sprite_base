use amethyst::{
	assets::{AssetStorage, Loader},
	prelude::*,
	renderer::{
		MaterialTextureSet,
		PngFormat,
		SpriteSheet,
		SpriteSheetFormat,
		SpriteSheetHandle,
		Texture,
		TextureMetadata,
	}
};

use util::check_asset_path_exists;

pub fn load_sprite_sheet (
		world: &mut World,
		texture_filename: String,
		spritesheet_config: String,
		texture_id: u64) -> SpriteSheetHandle {

	if !check_asset_path_exists(&texture_filename) {
		panic!("File not found: {}", texture_filename);
	}
	
	// Load the sprite sheet necessary to render the graphics.
	// The texture is the pixel data
	// `sprite_sheet` is the layout of the sprites on the image
	// `texture_handle` is a cloneable reference to the texture
	let texture_handle = {
		let loader = world.read_resource::<Loader>();
		let texture_storage = world.read_resource::<AssetStorage<Texture>>();
		loader.load(
			texture_filename,
			PngFormat,
			TextureMetadata::srgb_scale(),
			(),
			&texture_storage,
		)
	};

	// `texture_id` is a application defined ID given to the texture to store in the `World`.
	// This is needed to link the texture to the sprite_sheet.
	let mut material_texture_set = world.write_resource::<MaterialTextureSet>();
	material_texture_set.insert(texture_id, texture_handle);

	let loader = world.read_resource::<Loader>();
	let sprite_sheet_store = world.read_resource::<AssetStorage<SpriteSheet>>();
	loader.load(
		spritesheet_config, // Here we load the associated ron file
		SpriteSheetFormat,
		texture_id, // We pass it the ID of the texture we want it to use
		(),
		&sprite_sheet_store,
	)
}
