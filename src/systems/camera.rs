use std::hash::Hash;

use amethyst::{
	core::{
		shrev::{EventChannel, ReaderId},
		transform::{Transform},
		cgmath::{
			ortho,
			Vector2,
		},
	},
	ecs::prelude::{Join, Read, Resources, ReadExpect, System, SystemData, WriteStorage},
	input::{InputEvent, ScrollDirection, InputHandler},
	renderer::{
		Camera,
		ScreenDimensions,
		VirtualKeyCode,
	},
	winit::{
		MouseButton,
	}
};

use components::{
	CameraManager
};

fn lerp(v0: f32, v1: f32, t: f32) -> f32 {
    (1.0 - t) * v0 + t * v1  // maybe better
    //v0 + t * (v1 - v0)
}

pub struct CameraMovementSystem<AC>
where
	AC: Hash + Eq + 'static,
{
	event_reader: Option<ReaderId<InputEvent<AC>>>,
	mouse_left_down: bool,
	mouse_right_down: bool,
	mouse_moving: bool,
	mouse_delta: Vector2<f32>,
	mouse_pos: Vector2<f32>,
	zoom_update: bool,
	zoom_value: f32,
}

impl<AC> CameraMovementSystem<AC>
where
	AC: Hash + Eq + 'static,
{
	pub fn new() -> Self {
		CameraMovementSystem { 
			event_reader: None,
			mouse_left_down: false,
			mouse_right_down: false,
			mouse_moving: false,
			mouse_delta: Vector2::new(0.0, 0.0),
			mouse_pos: Vector2::new(0.0, 0.0),
			zoom_update: false,
			zoom_value: 0.0,
		}
	}
}

impl<'a, AC> System<'a> for CameraMovementSystem<AC>
where
	AC: Hash + Eq + Clone + Send + Sync + 'static,
{
	type SystemData = (
		Read<'a, EventChannel<InputEvent<AC>>>,
		Read<'a, InputHandler<String, String>>,
		ReadExpect<'a, ScreenDimensions>,
		WriteStorage<'a, Transform>,
		WriteStorage<'a, CameraManager>,
		WriteStorage<'a, Camera>,
	);

	fn run(&mut self, (events, input, dimensions, mut transforms, mut cam_managers, mut cameras): Self::SystemData) {
		
		self.mouse_moving = false;
		self.mouse_delta.x = 0.0;
		self.mouse_delta.y = 0.0;
		if let Some((pos_x, pos_y)) = input.mouse_position() {
			self.mouse_pos.x = pos_x as f32;
			self.mouse_pos.y = pos_y as f32;
		}

		for event in events.read(&mut self.event_reader.as_mut().unwrap()) {
			match *event {
				InputEvent::MouseButtonPressed(button) => match button {
					MouseButton::Left => {
						self.mouse_left_down = true;
					},
					MouseButton::Right => {
						self.mouse_right_down = true;
					}
					_ => (),
				}
				InputEvent::MouseButtonReleased(button) => match button {
					MouseButton::Left => {
						self.mouse_left_down = false;
					},
					MouseButton::Right => {
						self.mouse_right_down = false;
					}
					_ => (),
				}

				InputEvent::MouseMoved { delta_x, delta_y } => {
					self.mouse_moving = true;
					self.mouse_delta.x = delta_x as f32;
					self.mouse_delta.y = delta_y as f32;
				}			
				
				InputEvent::MouseWheelMoved(direction) => match direction {
					ScrollDirection::ScrollUp => {
						for (_camera, manager) in (&mut cameras, &mut cam_managers).join() {
							manager.zoom.x += manager.zoom_speed;
							manager.zoom.y += manager.zoom_speed;
							manager.tracking_speed += 0.25;
							if manager.tracking_speed >= 2.5 {
								manager.tracking_speed = 2.5;
							}
							self.zoom_update = true;
							self.zoom_value += 0.05;
							println!("{}", manager.tracking_speed);
						}
					}
					ScrollDirection::ScrollDown => {
						for (_camera, manager) in (&mut cameras, &mut cam_managers).join() {
							manager.zoom.x -= manager.zoom_speed;
							manager.zoom.y -= manager.zoom_speed;
							manager.tracking_speed -= 0.25;
							if manager.tracking_speed <= 0.5 {
								manager.tracking_speed = 0.5;
							}
							self.zoom_update = true;
							self.zoom_value -= 0.05;
							if self.zoom_value < 0.0 {
								self.zoom_value = 0.0;
							}
						}
					}
					_ => (),
				},

				InputEvent::KeyReleased{key_code, scancode: _} => {
					if key_code == VirtualKeyCode::R {
						for (_camera, manager) in (&mut cameras, &mut cam_managers).join() {
							manager.pos.x = 0.0;
							manager.pos.y = 0.0;
						}
					}
				}
				_ => (),
			}
		}
		for (transform, camera, manager) in (&mut transforms, &mut cameras, &mut cam_managers).join() {
			
			// Set the camera projection matrix to the current zoom level
			if self.zoom_update {
				// Camera size from unity
				/*
				let x = dimensions.width();
				let y = dimensions.height();
				// desired height of pixel square
				let s = 32.0;
				let camera_size = x / ((( x / y ) * 2 ) * s );
				let camera_size = y / (2 * s); //simplified
				*/

				let right = dimensions.width() - (dimensions.width() / 2.0 * self.zoom_value);
				let top = dimensions.height() - (dimensions.height() / 2.0 * self.zoom_value);

				let left = dimensions.width() - right;
				let bottom = dimensions.height() - top;

				//manager.pos.x = left / 2.0; //lerp(left, transform.translation.x, 0.05);
				//manager.pos.y = bottom / 2.0; //lerp(bottom, transform.translation.y, 0.05);

				println!("mouse-orthoL: {} mouse-orthoB: {}", lerp(left, self.mouse_pos.x, 0.05), lerp(bottom, self.mouse_pos.y, 0.05));
				
				camera.proj = ortho(left, right, bottom, top, 0.0, 13.0);
				println!("L:{}, R:{}, B:{}, T:{}, MX:{}, MY:{}", left, right, bottom, top, transform.translation.x, transform.translation.y);

			}

			if self.mouse_left_down && self.mouse_moving {
				manager.pos.x -= manager.tracking_speed * self.mouse_delta.x;
				manager.pos.y += manager.tracking_speed * self.mouse_delta.y;
				println!("{:?} {:?}", self.mouse_pos.x, self.mouse_pos.y);
			}

			if self.mouse_right_down {
				let x = lerp(transform.translation.x, self.mouse_pos.x - dimensions.width() / 2.0, 0.05);
				let y = lerp(transform.translation.y, self.mouse_pos.y - dimensions.height() / 2.0, 0.05);
				// self.zoom_value += 0.01;
				// if self.zoom_value >= 1.0 {
				// 	self.zoom_value = 1.0;
				// }
				println!("{:?} {:?} ", transform.translation.x, transform.translation.y);
				println!("{:?} {:?} {:?}", x, y, self.zoom_value);
				transform.translation.x = x;
				transform.translation.y = y;
			}

			transform.translation.x = manager.pos.x;
			transform.translation.y = manager.pos.y;
		}
		self.zoom_update = false;
	}

	fn setup(&mut self, res: &mut Resources) {
		Self::SystemData::setup(res);

		self.event_reader = Some(
			res.fetch_mut::<EventChannel<InputEvent<AC>>>()
				.register_reader(),
		);
	}
}