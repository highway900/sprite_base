use amethyst::core::cgmath::Deg;
use amethyst::core::{Transform};
use amethyst::core::specs::{Entity, Join, System, Entities, WriteStorage};

use components::{
	Movement
};

#[derive(Default)]
pub struct MovementSystem {
	to_delete: Vec<Entity>,
}

impl<'s> System<'s> for MovementSystem {
	type SystemData = (
		Entities<'s>,
		WriteStorage<'s, Transform>,
		WriteStorage<'s, Movement>,
	);

	fn run(&mut self, (entities, mut transforms, mut movers): Self::SystemData) {
		for (transform, mover, entity) in (&mut transforms, &mut movers, &*entities).join() {

			// TODO: here I could adjust course based on accumulated time
			mover.direction += 0.1;
			
			transform.set_rotation(Deg(0.), Deg(0.), Deg(mover.direction));
			transform.move_up(mover.speed);
			
			// Destory if outside the the game boundary
			// TODO: pass in boundry
			if transform.translation[1] < -64.0 || transform.translation[1] > 560.0 ||
					transform.translation[0] < -64.0 || transform.translation[0] > 560.0 {
				
				//println!("Destorying Entity: {:?}", entity);
				//self.to_delete.push(entity);
			}
		}

		for entity in self.to_delete.drain(..) {
			let _ = entities.delete(entity);
		}
	}
}