use config::{
	CameraConfig,
	SpriteConfig,
	GameConfig,
};

use components::{
	CameraManager,
	Movement
};

use amethyst::{
	core::{
		cgmath::{
			Deg,
			Ortho,
			Vector2,
			Vector3,
		},
		transform::{
			Transform,
		},
	},
	ecs::{
		Entity
	},
	input::{
		is_close_requested,
		is_key_down
	},
	prelude::*,
	renderer::{
		Camera,
		Projection,
		ScreenDimensions,
		SpriteRender,
		VirtualKeyCode,
	},
};

use sprites::{
	load_sprite_sheet,
};

const SPRITESHEET_ID_1: u64 = 0;

#[derive(PartialEq, Eq, Copy, Clone, Hash)]
pub enum AnimationId{}

fn create_sprite_entity(world: &mut World, sprite: SpriteConfig) -> Entity {
	let sprite_sheet_handle = load_sprite_sheet(
		world,
		String::from("sprites/ship_spritesheet.png"),
		String::from("sprites/sprites_config.ron"),
		SPRITESHEET_ID_1);
	
	// Move the sprite to the middle of the window
	let mut sprite_transform = Transform::default();
	sprite_transform.translation = sprite.pos;
	sprite_transform.set_rotation(Deg(0.), Deg(0.), Deg(sprite.rot_z));

	let sprite_render = SpriteRender {
		sprite_sheet: sprite_sheet_handle,
		sprite_number: sprite.sprite_id as usize,
		flip_horizontal: false,
		flip_vertical: false,
	};

	world
		.create_entity()
		.with(sprite_render)
		.with(sprite_transform)
		.with(Movement::new(sprite.rot_z, sprite.speed))
		.build()
}

fn initialise_sprites(world: &mut World) -> Vec<Entity> {
	let sprite_configs = {
		let config = &world.read_resource::<GameConfig>();
		//config.sprites.iter().map(|s| {(s.pos, s.sprite_id)}).collect::<Vec<_>>()
		config.sprites.clone()
	};

	let mut entities: Vec<Entity> = Vec::new();
	for sprite in sprite_configs {
		entities.push(create_sprite_entity(world, sprite));
	}

	entities
}

fn initialise_camera(world: &mut World) -> Entity {
	// For the depth, the additional + 1.0 is needed because the camera can see up to, but
	// excluding, entities with a Z coordinate that is `camera_z - camera_depth_vision`. The
	// additional distance means the camera can see up to just before -1.0 on the Z axis, so
	// we can view the sprite at 0.0.

	let (width, height) = {
		let dim = world.read_resource::<ScreenDimensions>();
		(dim.width(), dim.height())
	};

	let camera_manager = {
		let config = world.read_resource::<CameraConfig>();
		CameraManager::new(
			Vector3::new(config.pos[0], config.pos[1], config.pos[2]),
			Vector2::new(width, height),
			config.zoom_speed,
			config.tracking_speed)
	};

	let mut camera_transform = Transform::default();
	camera_transform.translation = camera_manager.pos;

	world
		.create_entity()
		.with(camera_transform)
		// Define the view that the camera can see. It makes sense to keep the `near` value as
		// 0.0, as this means it starts seeing anything that is 0 units in front of it. The
		// `far` value is the distance the camera can see facing the origin.
		.with(Camera::from(Projection::Orthographic(Ortho {
			left: 0.0,
			right: width,
			top: height,
			bottom: 0.0,
			near: 0.0,
			far: camera_manager.pos.z + 1.0,
		})))
		.with(camera_manager)
		.build()
}

pub struct SpritesBang {
	camera_main: Option<Entity>,
	entities: Vec<Entity>,
}

impl SpritesBang {
	pub fn new() -> Self {
		SpritesBang {
			camera_main: None,
			entities: Vec::new(),
		}
	}
}

impl<'a, 'b> SimpleState<'a, 'b> for SpritesBang {
	fn handle_event(&mut self, _data: StateData<GameData>, event: StateEvent) -> SimpleTrans<'a, 'b> {
		if let StateEvent::Window(event) = event {
			if is_close_requested(&event) || is_key_down(&event, VirtualKeyCode::Escape) {
				return Trans::Quit
			}
		}       
		Trans::None
	}

	fn update(&mut self, data: &mut StateData<GameData>) -> SimpleTrans<'a, 'b> {
		data.data.update(&data.world);
		Trans::None
	}

	fn on_start(&mut self, data: StateData<GameData>) {
		let StateData { world, .. } = data;
		
		let camera = initialise_camera(world);
		self.camera_main = Some(camera);
		
		self.entities = initialise_sprites(world);
	}
}
