extern crate amethyst;
#[macro_use]
extern crate serde_derive;

mod config;
mod components;
mod game;
mod systems;
mod sprites;
mod util;

use config::{
    CameraConfig,
    GameConfig,
};

use game::{
    AnimationId,
    SpritesBang
};

use amethyst::{
    animation::{
        AnimationBundle
    },
    core::{
        transform::{
            TransformBundle
        },
    },
    input::{
        InputBundle
    },
    prelude::*,
    renderer::{
        ALPHA,
        ColorMask,
        DisplayConfig,
        DrawSprite,
        Pipeline,
        RenderBundle,
        SpriteRender,
        Stage
    },
    utils::application_root_dir,
};

fn main() -> amethyst::Result<()> {
    amethyst::start_logger(Default::default());

    let app_root = application_root_dir();

    ////////// Configs

    let path = format!(
        "{}/resources/display_config.ron",
        app_root
    );
    let display_config = DisplayConfig::load(&path);

    let path = format!(
        "{}/resources/camera_config.ron",
        app_root
    );
    let camera_config = CameraConfig::load(&path);

    let path = format!(
        "{}/resources/game_config.ron",
        app_root
    );
    let game_config = GameConfig::load(&path);

    //////////

    let input_bundle = InputBundle::<String, String>::new();

    let pipe = Pipeline::build().with_stage(
        Stage::with_backbuffer()
            .clear_target([0.0, 0.0, 0.0, 1.0], 1.0)
            .with_pass(DrawSprite::new().with_transparency(ColorMask::all(), ALPHA, None))
    );

    let game_data =
        GameDataBuilder::default()
        .with_bundle(AnimationBundle::<AnimationId, SpriteRender>::new(
            "animation_control_system",
            "sampler_interpolation_system",
        ))?
        .with_bundle(
            // Handles transformations of textures
            TransformBundle::new()
                .with_dep(&["animation_control_system", "sampler_interpolation_system"]),
        )?
        .with_bundle(RenderBundle::new(pipe, Some(display_config)).with_sprite_sheet_processor())?
        .with_bundle(input_bundle)?
        .with(systems::CameraMovementSystem::<String>::new(), "camera_movement_system", &["input_system"])
        .with(systems::MovementSystem::default(), "MovementSystem", &[]);

    let assets_dir = format!("{}/assets/", app_root);

    let mut game = Application::build(assets_dir, SpritesBang::new())?
        .with_resource(camera_config)
        .with_resource(game_config)
        .build(game_data)?;

    game.run();

    Ok(())
}
