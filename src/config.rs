use amethyst::core::cgmath::{
	Vector3
};

#[derive(Debug, Default, Deserialize, Serialize)]
pub struct CameraConfig {
	pub pos: Vec<f32>,
	pub zoom: Vec<f32>,
	pub zoom_speed: f32,
	pub tracking_speed: f32,
}

#[derive(Debug, Default, Deserialize, Serialize)]
pub struct GameConfig {
	pub sprites: Vec<SpriteConfig>,
	pub score: u32,
}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct SpriteConfig {
	pub sprite_id: u32,
	pub pos: Vector3<f32>,
	pub rot_z: f32,
	pub speed: f32,
}